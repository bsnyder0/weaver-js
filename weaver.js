/* global $ */

var WeaverJS = function(container) {

    var _this = this;
    var _style = $('<style/>');
    var _loom = $(container);

    this.loom = _loom;

    var _harnesses = 4;
    var _tieups = [1,2,4,8,4,2];
    var _rows = [ 3, 6, 12, 9 ];
    
    var _colorsWarp = ["#ff0000", "#ff5500", "#ffaa00", "#ffff00", "#ffff55"];
    var _colorsWeft = ["#006600"];

    var _threadSpacing = 1;
    var _threadWidth = 5;
    var _backgroundColor = '#000000';

    var _loomThreadsWide = 50;
    var _loomThreadsDeep = 100;

    this.getTieups = function() {
        return _tieups;
    }
    this.getHarnessCount = function() {
        return _harnesses;
    }
    this.setTreadles = function(treadles) {
        if(typeof treadles.length!='undefined' && treadles.length>0) {
            _treadles = treadles;
            _harnesses = treadles[0].length;
            return true;
        } else {
            return false;
        }
    }

    this.setColorsWarp = function(colors) {
        if(typeof colors.length!='undefined' && colors.length>0) {
            _colorsWarp = colors;
            return true;
        } else {
            return false;
        }
    }

    this.setColorsWeft = function(colors) {
        if(typeof colors.length!='undefined' && colors.length>0) {
            _colorsWeft = colors;
            return true;
        } else {
            return false;
        }
    }

    this.setLoomThreadsWide = function(i) {
        if(i>0) {
            _loomThreadsWide = i;
            if($cWarp) {
                $cWarp.val(i);
            }
            return true;
        } else {
            return false;
        }
    }

    this.setThreadSpacing = function(i) {
        if(i>0) {
            _threadSpacing = i;
            return true;
        } else {
            return false;
        }
    }

    this.setThreadWidth = function(i) {
        if(i>0) {
            _threadWidth = i;
            return true;
        } else {
            return false;
        }
    }

    this.setLoomThreadsDeep = function(i) {
        if(i>0) {
            _loomThreadsDeep = i;
            if($cWeft) {
                $cWeft.val(i);
            }
            return true;
        } else {
            return false;
        }
    }

    this.buildStyle = function() {
        var styles = [];
        // warps
        var style = "";
        styles.push( _loom.selector + ", "+_loom.selector + " table { border-spacing: 0px; }" );
        styles.push( _loom.selector + " td { overflow: hidden; }" );
        styles.push( _loom.selector + " td.editable { background-color: white; }" );
        styles.push( _loom.selector + " td.not-editable { background-color: #cccccc; }" );
        styles.push( _loom.selector + " td.tieup-spacer { width: "+_threadSpacing+"px; height: "+_threadWidth+"px;  }" );
        styles.push( _loom.selector + " td.tieup-selector, "+_loom.selector + " td.treadle-selector { width: "+_threadWidth+"px; height: "+_threadWidth+"px; }" );
        styles.push( _loom.selector + " td.selected { width: "+_threadWidth+"px; height: "+_threadWidth+"px; background-color: black;  }" );
        styles.push( _loom.selector + " .cloth td:nth-child(odd), tr:nth-child(odd) td { width: "+_threadSpacing+"px; }" );
        styles.push( _loom.selector + " .cloth tr:nth-child(odd) td:nth-child(odd) { background-color: "+_backgroundColor+"; }" );
        for(var i in _colorsWarp) {
            style = _loom.selector + " .cloth td.color0-"+i+" { background-color: "+_colorsWarp[i]+";width: "+_threadWidth+"px; }";
            styles.push(style);
        }
        for(var i in _colorsWeft) {
            style = _loom.selector + " .cloth td.color1-"+i+" { background-color: "+_colorsWeft[i]+";height: "+_threadWidth+"px; }";
            styles.push(style);
        }
        _style.html(styles.join(''));
        $('body').append(_style);
    }

    this.buildLoom = function() {
        _loom.html('');
        var $topControls = $('<tr/>');
        $topControls
            .append('<td class="mix" />')
            .append('<td class="tieups" />');
        _loom.append($topControls);
        var $rows = $('<tr/>');
        $rows
            .append('<td class="treadles" />')
            .append('<td class="cloth" />');
        _loom.append($rows);
        $treadles = _loom.find('.treadles');
        $tieups = _loom.find('.tieups');
    }
    this.build = function() {
        _this.buildStyle();

        _this.buildLoom();
        _this.buildTieups();
        _this.buildTreadles();
        _this.buildCloth();
    }

    this.buildTieups = function() {
        $tieups = $('<table class="tieups" />');
        for(var h=0;h<_harnesses;h++) {
            var x = Math.pow(2,h);
            $harness = $('<tr data-h="'+h+'" />');
            for(var t=0;t<((_loomThreadsWide*2)+1);t++) {
                var onT = (t-1)/2;
                $td = $('<td/>');
                if(t%2==0) {
                    $td.addClass('tieup-spacer');
                } else {
                    var editable = onT == onT%_tieups.length;
                    $td.addClass(editable ? 'editable' : 'not-editable');
                    $td.data('h', h);
                    $td.data('twarp', onT);
                    $td.addClass('tieup-selector');
                    var tieup = _tieups[(onT)%_tieups.length];
                    if(tieup  & x) {
                        $td.addClass('selected');
                    }
                    if(editable) {
                        $td.on('click', function() {
                            _this.updateTieup($(this).data('twarp'), $(this).data('h'));
                        });
                    }
                }
                $harness.append($td);
            }
            $tieups.append($harness);
        }
        _loom.find('td.tieups').html($tieups);
    }

    this.updateTieup = function(x, y) {
        _tieups[x] = Math.pow(2,y);
        _this.buildTieups();
        _this.buildCloth();
    }

    this.buildTreadles = function() {
        $treadles = $('<table class="treadles" />');
        for(var r=0;r<((_loomThreadsDeep*2)+1);r++) {
            var onR = (r-1)/2;
            $row = $('<tr />');
            for(var h=0;h<_harnesses;h++) {
                $td = $('<td />');
                if(r%2==0) {
                    $td.addClass('treadle-spacer');
                } else {
                    var editable = onR == onR%_rows.length;
                    var row = _rows[(onR)%_rows.length];
                    $td.addClass(editable ? 'editable' : 'not-editable');
                    $td.data('tweft', onR);
                    $td.data('h', h);
                    $td.addClass('treadle-selector');
                    if(row & Math.pow(2,h)) {
                        $td.addClass('selected');
                    }
                    if(editable) {
                        $td.on('click', function() {
                            _this.updateTreadles($(this).data('tweft'), $(this).data('h'), $(this).hasClass('selected'));
                        });
                    }
                }
                $row.append($td);
            }
            $treadles.append($row);
        }
        _loom.find('td.treadles').html($treadles);
    }

    this.updateTreadles = function(x, y, i) {
        if(i) {
            // is selected, deselect
            _rows[x] -= Math.pow(2, y);
        } else {
            _rows[x] += Math.pow(2, y);
        }
        _this.buildTreadles();
        _this.buildCloth();
    }

    this.buildCloth = function() {
        /*
        var _tieups = [1,2,4,8,4,2];
        var _rows = [ 3, 6, 12, 9 ];
        */

        $cloth = $('<table class="cloth" />');
        for(var x=0;x<((_loomThreadsDeep*2)+1);x++) {       
            var onX = (x-1)/2; 
            $tr = $('<tr/>');
            for(var y=0; y<((_loomThreadsWide*2)+1);y++) {
                var onY = (y-1)/2;
                $td = $('<td/>');
                if(y%2==1 && x%2==0) {
                    $td.attr('class', 'color0-'+(onY%_colorsWarp.length));
                } else if(y%2==0 && x%2==1) {
                    $td.attr('class', 'color1-'+(onX%_colorsWeft.length));
                } else if (x%2==0 && y%2==0) {
                } else {

                    var tieup = _tieups[(onY)%_tieups.length]
                    var rowtreadles = _rows[(onX)%_rows.length];
                    // console.log(onX, onY, tieup, rowtreadles, tieup & rowtreadles)
                    // var warpOrWeft = _treadles[onX%_treadles.length][onY%_harnesses];
                    var warpOrWeft = tieup & rowtreadles;
                    $td.attr('class', 'color'+(warpOrWeft?1:0)+'-'+(warpOrWeft?onX%_colorsWeft.length:onY%_colorsWarp.length));
                }
                
                $tr.append($td);
            }
            $cloth.append($tr);
        }
        _loom.find('td.cloth').html($cloth);
        
    }

    var $cTieup, $cTreadle, $cHarness, $cWarp, $cWeft;
    this.bindTieup = function($c) {
        $cTieup = $c;
        $c
            .val(_tieups.length)
            .on('change', function() {
                if($(this).val()<1) {
                    $(this).val(1);
                }
                var x = $(this).val();
                if(x<_tieups.length) {
                    _tieups = _tieups.slice(0,x);
                }
                if(x>_tieups.length) {
                    while(_tieups.length!=x) {
                        _tieups.push(1);
                    }
                }
                _this.buildTieups();
                _this.buildCloth();
            });
    }

    this.bindTreadle = function($c) {
        $cTreadle = $c;
        $c
            .val(_rows.length)
            .on('change', function() {
                if($(this).val()<1) {
                    $(this).val(1);
                }
                var x = $(this).val();
                if(x<_rows.length) {
                    _rows = _rows.slice(0,x);
                }
                if(x>_rows.length) {
                    while(_rows.length!=x) {
                        _rows.push(1);
                    }
                }
                _this.buildTreadles();
                _this.buildCloth();
            });
    }

    this.bindHarness = function($c) {
        $cHarness = $c;
        $c
            .val(_harnesses)
            .on('change', function() {
                if($(this).val()<1) {
                    $(this).val(1);
                }
                var x = $(this).val();
                _harnesses = x;
                _this.buildTieups();
                _this.buildTreadles();
                _this.buildCloth();
            });
    }

    this.bindThreadsWarp = function($c) {
        $cWarp = $c;
        $c
            .val(_loomThreadsWide)
            .on('change', function() {
                if($(this).val()<1) {
                    $(this).val(1);
                }
                var x = $(this).val();
                _loomThreadsWide = x;
                _this.buildTieups();
                _this.buildTreadles();
                _this.buildCloth();
            });
    }
    this.bindThreadsWeft = function($c) {
        $cWeft = $c;
        $c
            .val(_loomThreadsDeep)
            .on('change', function() {
                if($(this).val()<1) {
                    $(this).val(1);
                }
                var x = $(this).val();
                _loomThreadsDeep = x;
                _this.buildTieups();
                _this.buildTreadles();
                _this.buildCloth();
            });
    }


}